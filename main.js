class board
{
    constructor(displayer) {
        this.container = document.getElementById("container");
        this.displayer = displayer;
      }
    makeRows(rows, cols) {
      this.rows=rows;
      this.cols=cols;
      let c=0;
      this.container.style.setProperty('--grid-rows', this.rows);
      this.container.style.setProperty('--grid-cols', this.cols);
      for (c = 0; c < (this.cols * this.rows); c++) {
        let num =c;
        let cell = document.createElement("div");
        cell.id=num;

        cell.innerText = (c + 1);
        cell.onclick = ()=>{
            
            if(this.checkVictory())
            {
                alert("victory!!!");
            }
            else
            {
                this.tilePress(num);
                if(this.checkVictory())
                {
                    alert("victory!");
                    this.displayer.addPlayer(this.makePlayer());
                    this.displayer.display();
                }
            }
        }
        this.container.appendChild(cell).className = "grid-item";
      };
    }
    
     getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
     addMissing()
    {
        let all = document.getElementsByClassName("grid-item");
        this.blankRow = this.getRandomInt(0,this.rows-1);
        this.blankCol = this.getRandomInt(0,this.cols-1);
        all[this.blankRow*this.cols+this.blankCol].innerText="";
    }

    countInversions()
    {
        let inversions=0;
        let all = document.getElementsByClassName("grid-item");
        for(let i=0;i<all.length;i++)
        {
            for(let j=i+1;j<all.length;j++)
            {
                if(all[i].innerText!=""&&all[j].innerText!=""&&parseInt(all[i].innerText)>parseInt(all[j].innerText))
                {

                    inversions++;
                }
            }
        }
        return inversions;
    }

    validateTable()
    {
        let inversions = this.countInversions();
        if(this.rows%2!=0&&inversions%2===0||this.rows%2===0 &&(inversions%2+this.blankRow+1)%2===0)
        {
            return true;
        }
        return false;
    }

    tilePress(index)
    {
        let col = index% this.cols;
        let row = Math.floor(index / this.cols);
        let all = document.getElementsByClassName("grid-item");
        if((row === this.blankRow && (col === this.blankCol+1||col === this.blankCol-1))
        || (col === this.blankCol && (row === this.blankRow+1 || row === this.blankRow-1)))
        {
            this.moves++;
            all[this.blankRow*this.cols+this.blankCol].innerText = all[row*this.cols + col].innerText;
            all[row*this.cols + col].innerText="";
            this.blankCol=col;
            this.blankRow=row;
        }
    }

    checkVictory()
    {
        if(this.countInversions()==0)
        {
            return true;
        }
        return false;
    }

    shuffle() {
        let vals = [...Array(this.cols*this.rows).keys()];
        for (let i = vals.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [vals[i], vals[j]] = [vals[j], vals[i]];
        }
        return vals;
    }

    assignValues()
    {
        let all = document.getElementsByClassName("grid-item");
        do
        {
            let vals = this.shuffle();
            console.log(vals);
            for(let i=0;i<this.cols*this.rows;i++)
            {
                all[i].innerText=vals[i]+1;
            }
            this.addMissing();
        }while(!(this.validateTable()));
    }

    wipeTable()
    {
        this.moves=0;
        let all = document.getElementsByClassName("grid-item");
        for(let i=0;i< this.rows*this.cols;i++)
        {
            const element = document.getElementById(i);
            element.remove();
        }
    }

    makePlayer()
    {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); 
        var yyyy = today.getFullYear();
        today = mm + '/' + dd + '/' + yyyy;
        let name = prompt("enter name");
        let moves = this.moves;
        let score = this.calculateScore(moves,this.rows*this.cols);
        let size = this.rows.toString()+"x"+this.cols.toString();
        return {"name":name,"score":score,"board_size":size,"moves":moves,"date":today};
    }


    calculateScore(moves,size)
    {
        return size/moves;
    }


}



class Button
{
    constructor(board) {
        this.container = document.getElementById("new-game");
        this.container.onclick= ()=>{
            board.wipeTable();
            let rows = prompt("Enter Grid rows:");
            let cols = prompt("Enter Grid columns:");
            board.makeRows(rows,cols);
            board.assignValues();
        }
      }
}


class displayResults
{
    constructor(topCount)
    {
        this.topCount=topCount;
        console.log(this.players);
        
        this.players=[];
        
    }

    addPlayer(player)
    {
        console.log(player);
        this.players.push(player);
    }

    display()
    {
       
        this.players = this.players.sort(function(a,b){
            return(b.score-a.score);
        })
        let topPlayers=[];
        
        for(let i=0;i<this.players.length&&i<this.topCount;i++)
        {
            let sentence =
             "name: "+ this.players[i].name
              +" score: "+ this.players[i].score
               +" boardSize: "+this.players[i].board_size
                +" moves"+this.players[i].moves
                +" date:"+this.players[i].date;
           topPlayers.push(sentence);
           
        }   
        console.log(topPlayers); 
        alert("top-users: "+topPlayers);
    }

   
    
}





displayer = new displayResults(5);
board = new board(displayer);

b = new Button(board);
